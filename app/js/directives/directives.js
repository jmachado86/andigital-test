var exampleDirectives = angular.module('exampleDirectives', []);

exampleDirectives.directive('maps',function(){
    function link($scope, $element, attrs, ngModelCtrl) { 

        function initializeMap(position){        
          var mapOptions = {
            zoom: 8,
            center: position
          };
          console.log(mapOptions);
            map = new google.maps.Map(document.getElementById('map'),mapOptions);   
        }

        function detectLocation (){
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    console.log(position);
                    var pos = new google.maps.LatLng(position.coords.latitude,
                                       position.coords.longitude);
                    initializeMap(pos);

                });
            }
            else{
                var pos = google.maps.LatLng(51.424997, -0.019225);
                initializeMap(pos);                
            }
        }

        detectLocation();
    }

    return {
        restrict: 'E',
        templateUrl: 'partials/custom/maps.html',
        link: link
    };
});





