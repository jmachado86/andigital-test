var main = {
    venues : [],
	mainController: function($scope,$location,fourSquareService) {
        $scope.errors = [];
        $scope.queryVenues = function(){
            $scope.errors = [];
            fourSquareService.search({
                query: $scope.query
            },
            function(response){
                main.cleanVenues();
                var venues = response.response.venues;
                if (venues !== null && venues.length > 0){
                    var latLng = map.panTo(new google.maps.LatLng(venues[0].location.lat,venues[0].location.lng));
                    for (var i = 0 ; i < venues.length; i++) {
                        var marker = new google.maps.Marker({
                            position: new google.maps.LatLng(venues[i].location.lat,venues[i].location.lng),
                            map: map,
                            title: venues[i].name
                        });

                        var infowindow = new google.maps.InfoWindow({ 
                            content: main.buildInfoWindow(venues[i])
                        });

                        google.maps.event.addListener(marker, 'click', function() {
                            infowindow.open(map,this);
                        });
                        main.venues.push(marker);
                    }   
                }
                else
                    $scope.errors.push("No places where found, please try again with a different query");

            },
            function(response){
                $scope.errors.push(response.meta.errorDetail);
            })
        }
	},
    cleanVenues: function(){
        if (main.venues !== null && main.venues.length > 0){
            for (var i = 0; i < main.venues.length; i++ ) {
                main.venues[i].setMap(null);
            }
            main.venues.length = 0;
        }
    },
    buildInfoWindow: function(venue){
        return "<h3><a href='"+venue.url+"'>"+venue.name+"</a></h3><p>"+venue.categories[0].shortName +"</p><p>"
        +venue.location.formattedAddress[0] 
        + venue.location.formattedAddress[1]+"</p>";
    }
};