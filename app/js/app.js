var startupLoans = angular.module('startupLoans', [ 'ngRoute',
                                                    'ngAnimate',
                                                    'ngCookies',
                                                    'exampleControllers',
                                                    'exampleServices',
                                                    'exampleDirectives']);	

startupLoans.config(['$routeProvider','$locationProvider',
  function($routeProvider,$locationProvider) {
  	$routeProvider.when('/', {
        templateUrl: 'partials/venue/index.html',
        controller: 'mainController'
      }).otherwise({ redirectTo: '/'})
  }]);