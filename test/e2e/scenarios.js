'use strict';

/* http://docs.angularjs.org/guide/dev_guide.e2e-testing */

describe('Example', function() {

  it('should redirect app to index.html', function() {
    browser.get('app/');
    browser.getLocationAbsUrl().then(function(url) {
        expect(url.split('#')[1]).toBe('/');
      });
  });

  describe('it should display the main view', function(){
    browser.get('app/');
  });

});
