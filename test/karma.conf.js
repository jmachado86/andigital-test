module.exports = function(config){
  config.set({

    basePath : '../',

    files : [
      'app/bower_components/angular/angular.js',
      'app/bower_components/angular-route/angular-route.js',
      'app/bower_components/angular-resource/angular-resource.js',
      'app/bower_components/angular-animate/angular-animate.js',
      'app/bower_components/angular-mocks/angular-mocks.js',
      // 'app/js/**/*.js',
      'app/js/main.js',
      'app/js/controllers/userController.js',
      'app/js/controllers/commonsController.js',
      'app/js/controllers/sessionController.js',
      'app/js/controllers/entityController.js',
      'app/js/controllers/documentController.js',
      'app/js/controllers/controller.js',
      'app/js/services/services.js',
      'app/js/directives/directives.js',
      'app/js/app.js',
      'test/unit/**/*.js'
    ],

    autoWatch : true,

    frameworks: ['jasmine'],

    browsers : ['Chrome'],

    plugins : [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-jasmine'
            ],

    junitReporter : {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};